#include <string>
using namespace std;

class Zooshop
{
private:
	std::string animal,
		sex,
		name;
	int price,
		count;

public:
	Zooshop();
	Zooshop(string newAnimal, string newSex, string newName, int newPrice, int newCount);
	Zooshop(const Zooshop &animal);

	~Zooshop();

	void setData(std::string newAnimal, std::string newSex, std::string newName, int newPrice, int newCount);
	void getData();

	void changePar(std::string namePar, std::string newPar);
	void changePar(std::string namePar, int newPar);

};
