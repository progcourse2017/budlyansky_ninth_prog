#include "stdafx.h"
#include <iostream>

#include "zooClass.h"

Zooshop::Zooshop()
{
	animal = "";
	sex = "";
	name = "";
	price = 0;
	count = 0;
};

Zooshop::Zooshop(string newAnimal, string newSex, string newName, int newPrice, int newCount)
{
	animal = newAnimal;
	sex = newSex;
	name = newName;
	price = newPrice;
	count = newCount;
};

Zooshop::~Zooshop() {};

void Zooshop::setData(std::string newAnimal, std::string newSex, std::string newName, int newPrice, int newCount)
{
	animal = newAnimal;
	sex = newSex;
	name = newName;
	price = newPrice;
	count = newCount;
};

void Zooshop::getData()
{
	std::cout << "ANIMAL: " << animal << "\n"
		<< "SEX: " << sex << "\n"
		<< "ANIMAL NAME: " << name << "\n"
		<< "PRICE: " << price << "\n"
		<< "COUNT: " << count;
};

void Zooshop::changePar(std::string namePar, std::string newPar)
{
	if (namePar == "animal")
	{
		animal = newPar;
	}
	else if (namePar == "sex")
	{
		sex = newPar;
	}
	else if (namePar == "name")
	{
		name = newPar;
	}

};

void Zooshop::changePar(std::string namePar, int newPar)
{
	if (namePar == "price")
	{
		price = newPar;
	}
	else if (namePar == "count")
	{
		count = newPar;
	}

};