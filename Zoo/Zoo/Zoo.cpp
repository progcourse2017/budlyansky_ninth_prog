// Zoo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

#include "zooClass.h"
using namespace std;

int main()
{
	Zooshop animal;

	Zooshop newAnimal("cat", "girl", "Stremyanka", 1000, 1);

	string namePar, newParS;

	int newParI;

	animal.setData("dog", "yeahboy", "Boyarishnik", 150, 7);

	cout << "\n********************************\n";
	animal.getData();
	cout << "\n********************************\n";
	newAnimal.getData();
	cout << "\n********************************\n";



	cout << "Change the parametr what would you like to change\n"
		<< "(animal,sex,name,price,count):";
	cin >> namePar;

	cout << "Enter the new " << namePar << ":";

	if (namePar == "price" || namePar == "count")
	{
		cin >> newParI;
		animal.changePar(namePar, newParI);
	}
	else
	{
		cin >> newParS;
		animal.changePar(namePar, newParS);
	}


	cout << "\n\n\n";

	animal.getData();

	cout << "\n********************************\n";

	newAnimal = animal;

	newAnimal.getData();

	system("pause");

	return 0;
}
